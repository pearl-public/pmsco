#!/usr/bin/env python

"""
@file package distribution information.

preliminary - not tested.
"""

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'name': "pmsco",
    'description': "PEARL Multiple-Scattering Cluster Calculation and Structural Optimization",
    'url': "https://git.psi.ch/pearl/pmsco",
    'author': "Matthias Muntwiler",
    'author_email': "matthias.muntwiler@psi.ch",
    'version': '0.1',
    'packages': ['pmsco'],
    'scripts': [],
    'install_requires': ['numpy','periodictable','statsmodels','mpi4py','nose', 'scipy']
}

setup(**config)
