SHELL=/bin/sh

# makefile for all programs, modules and documentation
#
# required libraries for LOESS module: libblas, liblapack, libf2c
# (you may have to set soft links so that linker finds them)
#
# on shared computing systems (high-performance clusters) 
# you may have to switch the environment before running this script.
#
# note: the public distribution does not include third-party code
# (EDAC in particular) because of incompatible license terms.
# please obtain such code from the original authors
# and copy it to the proper directory before compilation.
#
# the MSC and MUFPOT programs are currently not used.
# they are not built by the top-level targets all and bin.
#
# the make system uses the compiler executables of the current environment.
# to override the executables, you may set the following variables.
# to switch between python versions, however, the developers recommend miniconda.
#
# PYTHON = python executable (default: python)
# PYTHONOPTS = python options (default: none)
# CC = C and Fortran compiler executable (default: gcc)
# CCOPTS = C compiler options (default: none)
# CXX = C++ compiler executable (default: g++)
# CXXOPTS = C++ compiler options (default: none)
#
# make all PYTHON=/usr/bin/python2.7
#
# or:
#
# export PYTHON=/usr/bin/python2.7
# make all
#

.PHONY: all bin docs clean edac loess msc mufpot phagen

PMSCO_DIR = pmsco
DOCS_DIR = docs

all: edac loess phagen docs

bin: edac loess phagen

edac loess msc mufpot phagen:
	$(MAKE) -C $(PMSCO_DIR)

docs:
	$(MAKE) -C $(DOCS_DIR)

clean:
	$(MAKE) -C $(PMSCO_DIR) clean
	$(MAKE) -C $(DOCS_DIR) clean
