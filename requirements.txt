python >= 3.6
attrdict
fasteners
numpy >= 1.13
periodictable
statsmodels
mpi4py
nose
mock
scipy
matplotlib
future
swig
gitpython
commentjson
