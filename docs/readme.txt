To compile the source code documentation in HTML format, 
you need the following packages.
They are available from Linux distributions unless noted otherwise.

GNU make
doxygen
python
doxypypy (pip)
graphviz
java JRE
plantuml (download from plantuml.com)

export the location of plantuml.jar in the PLANTUML_JAR_PATH environment variable.

go to the `docs` directory and execute `make html`.

open `docs/html/index.html` in your browser.
