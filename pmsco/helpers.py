"""
@package pmsco.helpers
helper classes

a collection of small and generic code bits mostly collected from the www.

"""


class BraceMessage(object):
    """
    a string formatting proxy class useful for logging and exceptions.

    use BraceMessage("{0} {1}", "formatted", "message")
    in place of "{0} {1}".format("formatted", "message").
    the advantage is that the format method is called only if the string is actually used.
    """
    def __init__(self, fmt, *args, **kwargs):
        self.fmt = fmt
        self.args = args
        self.kwargs = kwargs

    def __str__(self):
        return self.fmt.format(*self.args, **self.kwargs)
