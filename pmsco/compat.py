"""
@package pmsco.compat
compatibility code

code bits to provide compatibility for different python versions.
currently supported 2.7 and 3.6.
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from io import open as io_open


def open(fname, mode='r', encoding='latin1'):
    """
    open a data file for read/write/append using the default str type

    this is a drop-in for io.open
    where data is exchanged via the built-in str type of python,
    whether this is a byte string (python 2) or unicode string (python 3).

    the file is assumed to be a latin-1 encoded binary file.

    @param fname: file name and path
    @param mode: 'r', 'w' or 'a'
    @param encoding: 'latin1' (default), 'ascii' or 'utf-8'
    @return file handle
    """
    if isinstance(b'b', str):
        # python 2
        mode += 'b'
        kwargs = {}
    else:
        # python 3
        mode += 't'
        kwargs = {'encoding': encoding}

    return io_open(fname, mode, **kwargs)
