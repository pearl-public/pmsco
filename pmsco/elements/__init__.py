"""
@package pmsco.elements
extended properties of the elements

this package extends the element table of the `periodictable` package
(https://periodictable.readthedocs.io/en/latest/index.html)
by additional attributes like the electron binding energies.

the package requires the periodictable package (https://pypi.python.org/pypi/periodictable).


@author Matthias Muntwiler

@copyright (c) 2020 by Paul Scherrer Institut @n
Licensed under the Apache License, Version 2.0 (the "License"); @n
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  http://www.apache.org/licenses/LICENSE-2.0
"""

import periodictable.core


def _load_binding_energy():
    """
    delayed loading of the binding energy table.
    """
    from . import bindingenergy
    bindingenergy.init(periodictable.core.default_table())


def _load_photoionization():
    """
    delayed loading of the binding energy table.
    """
    from . import photoionization
    photoionization.init(periodictable.core.default_table())


periodictable.core.delayed_load(['binding_energy'], _load_binding_energy)
periodictable.core.delayed_load(['photoionization'], _load_photoionization)
