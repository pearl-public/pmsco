"""
@package pmsco.__main__
__main__ module

thanks to this small module you can go to the project directory and run PMSCO like this:
@verbatim
python pmsco [pmsco-arguments]
@endverbatim
"""

from pathlib import Path
import sys

pmsco_root = Path(__file__).resolve().parent.parent
if str(pmsco_root) not in sys.path:
    sys.path.insert(0, str(pmsco_root))


if __name__ == '__main__':
    import pmsco.pmsco
    pmsco.pmsco.main()
