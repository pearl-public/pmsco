#!/usr/bin/env python

"""
setup.py file for EDAC
"""

from distutils.core import setup, Extension


edac_module = Extension('_edac',
                           sources=['edac_all.cpp', 'edac_all.i'],
                           swig_opts=['-c++']
                           )

setup (name = 'edac',
       version = '0.1',
       author      = "Matthias Muntwiler",
       description = """EDAC module in Python""",
       ext_modules = [edac_module],
       py_modules = ["edac"], 
       requires=['numpy']
       )

