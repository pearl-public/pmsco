#!/usr/bin/env python

"""
@package loess.setup
setup.py file for LOESS

the LOESS code included here was developed at Bell Labs by
William S. Cleveland, Eric Grosse, Ming-Jen Shyu,
and is dated 18 August 1992.
the code is available in the public domain
from http://www.netlib.org/a/dloess.
see the README file for details.

the Python wrapper was set up by M. Muntwiler
with the help of the SWIG toolkit
and other incredible goodies available in the Linux world.

@bug numpy.distutils.build_src in python 2.7 treats all Fortran files with f2py
so that they are compiled via both f2py and swig.
this produces extra object files which cause the linker to fail.
to fix this issue, this module hacks the build_src class.
this hack does not work with python 3. perhaps it's even unnecessary.

@author Matthias Muntwiler

@copyright (c) 2015-18 by Paul Scherrer Institut @n
Licensed under the Apache License, Version 2.0 (the "License"); @n
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  http://www.apache.org/licenses/LICENSE-2.0
"""

import numpy
try:
    numpy_include = numpy.get_include()
except AttributeError:
    numpy_include = numpy.get_numpy_include()

def configuration(parent_package='', top_path=None):
    from numpy.distutils.misc_util import Configuration
    config = Configuration('loess', parent_package, top_path)
    lib = ['blas', 'm', 'f2c']
    src = ['loess.c', 'loessc.c', 'predict.c', 'misc.c', 'loessf.f', 'dqrsl.f', 'dsvdc.f', 'fix_main.c', 'loess.i']
    inc_dir = [numpy_include]
    config.add_extension('_loess',
                         sources=src,
                         libraries=lib,
                         include_dirs=inc_dir
                         )
    return config

def ignore_sources(self, sources, extension):
    return sources

if __name__ == '__main__':
    try:
        from numpy.distutils.core import numpy_cmdclass
        numpy_cmdclass['build_src'].f2py_sources = ignore_sources
    except ImportError:
        pass
    from numpy.distutils.core import setup
    setup(**configuration(top_path='').todict())

