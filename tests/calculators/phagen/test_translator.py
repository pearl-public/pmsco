"""
@package tests.calculators.phagen.test_translator
unit tests for pmsco.calculators.phagen.translator

the purpose of these tests is to check whether the code runs as expected in a particular environment.

to run the tests, change to the directory which contains the tests directory, and execute =nosetests=.

@pre nose must be installed (python-nose package on Debian).

@author Matthias Muntwiler, matthias.muntwiler@psi.ch

@copyright (c) 2015-19 by Paul Scherrer Institut @n
Licensed under the Apache License, Version 2.0 (the "License"); @n
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  http://www.apache.org/licenses/LICENSE-2.0
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import unittest
from pmsco.calculators.phagen import translator


class TestModule(unittest.TestCase):
    def setUp(self):
        # before each test method
        pass

    def tearDown(self):
        # after each test method
        pass

    @classmethod
    def setup_class(cls):
        # before any methods in this class
        pass

    @classmethod
    def teardown_class(cls):
        # teardown_class() after any methods in this class
        pass

    def test_state_to_edge(self):
        self.assertEqual(translator.state_to_edge('1s'), 'k')
        self.assertEqual(translator.state_to_edge('2s'), 'l1')
        self.assertEqual(translator.state_to_edge('3s'), 'm1')
        self.assertEqual(translator.state_to_edge('4s'), 'n1')
        self.assertEqual(translator.state_to_edge('5s'), 'o1')

        self.assertEqual(translator.state_to_edge('2p'), 'l2')
        self.assertEqual(translator.state_to_edge('3p'), 'm2')
        self.assertEqual(translator.state_to_edge('4p'), 'n2')
        self.assertEqual(translator.state_to_edge('5p'), 'o2')

        self.assertEqual(translator.state_to_edge('2p1/2'), 'l2')
        self.assertEqual(translator.state_to_edge('3p1/2'), 'm2')
        self.assertEqual(translator.state_to_edge('4p1/2'), 'n2')
        self.assertEqual(translator.state_to_edge('5p1/2'), 'o2')

        self.assertEqual(translator.state_to_edge('2p3/2'), 'l3')
        self.assertEqual(translator.state_to_edge('3p3/2'), 'm3')
        self.assertEqual(translator.state_to_edge('4p3/2'), 'n3')
        self.assertEqual(translator.state_to_edge('5p3/2'), 'o3')

        self.assertEqual(translator.state_to_edge('3d'), 'm4')
        self.assertEqual(translator.state_to_edge('4d'), 'n4')
        self.assertEqual(translator.state_to_edge('5d'), 'o4')

        self.assertEqual(translator.state_to_edge('3d3/2'), 'm4')
        self.assertEqual(translator.state_to_edge('4d3/2'), 'n4')
        self.assertEqual(translator.state_to_edge('5d3/2'), 'o4')

        self.assertEqual(translator.state_to_edge('3d5/2'), 'm5')
        self.assertEqual(translator.state_to_edge('4d5/2'), 'n5')
        self.assertEqual(translator.state_to_edge('5d5/2'), 'o5')
