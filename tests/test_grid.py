"""
@package tests.test_grid
unit tests for pmsco.optimizers.grid

the purpose of these tests is to help debugging the code.

to run the tests, change to the directory which contains the tests directory, and execute =nosetests=.

@pre nose must be installed (python-nose package on Debian).

@author Matthias Muntwiler, matthias.muntwiler@psi.ch

@copyright (c) 2015-19 by Paul Scherrer Institut @n
Licensed under the Apache License, Version 2.0 (the "License"); @n
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  http://www.apache.org/licenses/LICENSE-2.0
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np
import random
import unittest

import pmsco.optimizers.grid as mo
import pmsco.project as mp


class TestPopulation(unittest.TestCase):
    def setUp(self):
        random.seed(0)
        self.model_space = mp.ModelSpace()

        self.model_space.add_param('A', 1.5, 1.0, 2.0, 0.2)
        self.model_space.add_param('B', 2.5, 2.0, 3.0, 0.25)
        self.model_space.add_param('C', 3.5, 3.5, 3.5, 0.0)
        self.expected_popsize = 30
        self.expected_names = ('_model', '_rfac', 'A', 'B', 'C')

        self.pop = mo.GridPopulation()

    def tearDown(self):
        # after each test method
        self.pop = None

    @classmethod
    def setup_class(cls):
        # before any methods in this class
        pass

    @classmethod
    def teardown_class(cls):
        # teardown_class() after any methods in this class
        pass

    def test_setup(self):
        self.pop.setup(self.model_space)
        self.assertEqual(self.pop.positions.dtype.names, self.expected_names)
        self.assertEqual(self.pop.positions.shape, (self.expected_popsize,))
        self.assertEqual(self.pop.model_count, self.expected_popsize)
        check = np.arange(self.expected_popsize)
        np.testing.assert_array_equal(self.pop.positions['_model'], check)
        check = np.ones(self.expected_popsize) * 2.1
        np.testing.assert_array_almost_equal(self.pop.positions['_rfac'], check)


if __name__ == '__main__':
    unittest.main()
